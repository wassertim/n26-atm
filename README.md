
**ATM**

My implementation of an ATM uses angular2 and plain javascript. I can honestly say that before this, I had not used angular2 with plain JS together, so it was a special fun for me :) I also considered using angularJS 1.5 as a SPA framework, but decided that an angular2 implementation would be clearer. Here are some details about the implementation:

- Each screen has its own route ('/welcome', '/pin' ...)
- The timeouts are hidden inside a CardService implementation
- The card insertion is implemented with the html5 drag and drop
- The num pad and the card reader are separate components
- Twitter bootstrap for better styling

**The Num Pad Component**

The num-pad was implemented as as separate component, because it is used 2 times: first - for entering a pin, second - for entering a custom amount. It also makes the code clearer.

**The Card Reader Component**

The card reader component was implemented only to make the code more understandable.

Since each screen has it's own route it is easy just to go to the '/withdraw' page and get the money without entering a pin. Checking the pin on each page was not implemented because it was not a requirement. All the input operation are done with the num-pad, so the entering a route directly could not be possible without an extra computer keyboard in real world situation :)

Before start

- npm install
- npm start

Run the website: localhost:3000
