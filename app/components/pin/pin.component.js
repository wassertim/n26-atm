(function(app) {
  app.PinComponent =
    ng.core.Component({
      selector: 'pin',
      templateUrl: '/app/components/pin/pin.component.html',
      directives: [app.NumPadComponent],
      providers: [app.CardService]
    })
    .Class({
      constructor: [ng.router_deprecated.Router, app.CardService, function(router, cardService) {
        var that = this;
        //This is just for showing a "waiting message" when it is true
        this.pinBeingVerified = false;

        //We listen the Enter event from the num pad
        this.onEnter = function(pinCode){
          that.pinBeingVerified = true;
          var success = function() {
            that.pinBeingVerified = false;
            //We navigate to the next screen if the pin is correct
            router.navigate(['AmountList']);
          };
          var failure = function () {
            that.pinBeingVerified = false;
            alert('The pin-code you entered is incorrect. Please try again');
          };
          //It only verifies a pin entered with the num pad
          cardService.verifyPinCode(pinCode).then(success, failure);
        }
      }]
    });

})(window.app || (window.app = {}));
