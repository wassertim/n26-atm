(function(app) {
  app.AmountListComponent =
    ng.core.Component({
      selector: 'amount-list',
      templateUrl: '/app/components/amount-list/amount-list.component.html',
      directives: [ng.router_deprecated.RouterLink]
    })
    .Class({
      constructor: [ng.router_deprecated.Router, function(router) {
        //We have a predefined list of amounts here.
        this.amountList = [50, 100, 200, 300, 500, 1000];

        //It will handle the click event of the "custom amount" button
        this.goToCustomAmount = function() {
          router.navigate(['CustomAmount']);
        };
      }]
    });

})(window.app || (window.app = {}));
