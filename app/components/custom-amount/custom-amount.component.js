(function(app) {
  app.CustomAmountComponent =
    ng.core.Component({
      selector: 'amount-list',
      templateUrl: '/app/components/custom-amount/custom-amount.component.html',
      directives: [ng.router_deprecated.RouterLink, app.NumPadComponent]
    })
    .Class({
      constructor: [ng.router_deprecated.Router, function(router) {
        //It uses the num pad, so onEnter we get the value entered
        this.onEnter = function(value) {
          if(value) {
            //We good to deliver the money
            router.navigate(['Withdraw', {amount: value}]);
          }
        };
      }]
    });

})(window.app || (window.app = {}));
