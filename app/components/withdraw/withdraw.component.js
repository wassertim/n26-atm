(function(app) {
  app.WithdrawComponent =
    ng.core.Component({
      selector: 'withdraw',
      templateUrl: '/app/components/withdraw/withdraw.component.html',
      providers: [app.CardService]
    })
    .Class({
      constructor: [
        ng.router_deprecated.Router,
        ng.router_deprecated.RouteParams,
        app.CardService,
        function(router, route, cardService) {
          //We get the amount from the previous screen
          this.amount = route.params['amount'];
          //Prepare the money
          cardService.withdraw(this.amount).then(function() {
            //Go to the next screen
            router.navigate(['GetYourCard']);
          });
        }
      ]
    });

})(window.app || (window.app = {}));
