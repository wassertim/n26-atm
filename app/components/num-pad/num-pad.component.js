(function(app) {
  app.NumPadComponent =
    ng.core.Component({
      selector: 'num-pad',
      templateUrl: '/app/components/num-pad/num-pad.component.html',
      outputs : ['onEnter']
    })
    .Class({
      constructor: function() {
        this.inputField = '';
        this.onEnter = new ng.core.EventEmitter();
        this.numPad = [
          [1, 2, 3, 'cancel'],
          [4, 5, 6, 'clear'],
          [7, 8, 9, 'enter'],
          [null, 0, null, null]
        ];
        this.onClick = function(key) {
          if((/[0-9]/g).test(key)) {            
            this.inputField += key;
          }
          if(key === 'enter'){
            this.onEnter.emit(this.inputField);
            this.inputField = '';
          }
          if(key === "clear") {
            this.inputField = '';
          }
          if(key === "cancel") {
            this.inputField = this.inputField.substring(0, this.inputField.length - 1);
          }
        }
      }
    });

})(window.app || (window.app = {}));
