(function(app) {
  app.GetYourCardComponent =
    ng.core.Component({
      selector: 'withdraw',
      templateUrl: '/app/components/get-your-card/get-your-card.component.html',
      providers: [app.CardService]
    })
    .Class({
      constructor: [
        ng.router_deprecated.Router,
        ng.router_deprecated.RouteParams,
        app.CardService,
        function(router, route, cardService) {
          this.getCard = function() {
            //This function will return the card when user clicks the button
            cardService.returnCard().then(function() {
              //It will go back to the Welcome screen
              router.navigate(['Welcome']);
            });
          }
        }
      ]
    });

})(window.app || (window.app = {}));
