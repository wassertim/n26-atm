//This component hides the implementation of a card reader,
//because we are not sure what should we use button or drag and drop
//this implementation uses the drag and drop but could be easily changed to a button
(function(app) {
  app.CardReaderComponent =
    ng.core.Component({
      selector: 'card-reader',
      templateUrl: '/app/components/card-reader/card-reader.component.html',
      outputs : ['cardInserted']
    })
    .Class({
      constructor: [ng.router_deprecated.Router, app.CardService, function(router, cardService) {
        this.cardInserted = new ng.core.EventEmitter();
        this.allowDrop = function(ev) {
          ev.preventDefault();
        };

        this.drag = function(ev) {
          ev.dataTransfer.setData("card-element-id", ev.target.id);
        };

        this.drop = function(ev) {
          ev.preventDefault();
          var cardElementId = ev.dataTransfer.getData("card-element-id");
          ev.target.appendChild(document.getElementById(cardElementId));
          //The event that we call.
          //Could be easily used with a button.click if there were any
          this.cardInserted.emit('');
        };
      }]
    });

})(window.app || (window.app = {}));
