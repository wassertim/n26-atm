(function(app) {
  app.WelcomeComponent =
    ng.core.Component({
      selector: 'welcome',
      templateUrl: '/app/components/welcome/welcome.component.html',
      directives: [app.CardReaderComponent],
      providers: [app.CardService]
    })
    .Class({
      constructor: [ng.router_deprecated.Router, app.CardService, function(router, cardService) {
        //Here we handle the cardInserted event from our skimmer
        this.onCardInserted = function() {
          this.cardIsBeingVerified = true;
          //Then we verify the card so it is valid
          cardService.verifyCard().then(function(){
              //And navigate to the next screen
              router.navigate(['Pin']);
          });
        };
      }]
    });

})(window.app || (window.app = {}));
