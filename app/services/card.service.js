(function(app){
  var emptyPromise = function(timeoutValue) {
    //Just empty promise to mock the service behavior
    return new Promise(function(resolve, reject) {
        setTimeout(function() {
            resolve();
        }, timeoutValue);
    });
  }
  app.CardService = ng.core.Class({
    constructor: function() {},
    verifyCard: function() {
      return emptyPromise(2000);
    },
    withdraw: function(amount) {
      return emptyPromise(2000);
    },
    returnCard: function() {
      return emptyPromise(500);
    },
    verifyPinCode: function(pinCode) {
      return (new Promise(function(resolve, reject) {
          setTimeout(function() {
            //Check the pin
            if (pinCode === '1234') {
                resolve();
            } else {
                reject();
            }
          }, 2000);
      }));
    }
  });
})(window.app || (window.app = {}));
