
(function(app) {
  app.AppComponent =
    ng.core.Component({
      selector: 'my-app',
      templateUrl: '/app/app.component.html',
      directives: [
        ng.router_deprecated.ROUTER_DIRECTIVES,
        app.HeaderComponent,
        app.WelcomeComponent,
        app.PinComponent,
        app.WithdrawComponent
      ],
      providers: [ng.router_deprecated.ROUTER_PROVIDERS]
    })
    .Class({
      constructor: function() {
      }
    });
  //Here we define our screens and routes
  app.AppComponent = ng.router_deprecated.RouteConfig([
    {
      path: '/',
      component: app.WelcomeComponent,
      name: 'Welcome'
    },
    {
      path: '/pin',
      component: app.PinComponent,
      name: 'Pin'
    },
    {
      path: '/amount-list',
      component: app.AmountListComponent,
      name: 'AmountList'
    },
    {
      path: '/withdraw',
      component: app.WithdrawComponent,
      name: 'Withdraw'
    },
    {
      path: '/custom-amount',
      component: app.CustomAmountComponent,
      name: 'CustomAmount'
    },
    {
      path: '/get-your-card',
      component: app.GetYourCardComponent,
      name: 'GetYourCard'
    }
  ])(app.AppComponent);
})(window.app || (window.app = {}));
