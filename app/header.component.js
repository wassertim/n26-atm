(function(app) {
  app.HeaderComponent =
    ng.core.Component({
      selector: 'header',
      templateUrl: 'app/header.component.html',
      directives: [ng.router_deprecated.RouterLink]
    })
    .Class({
      constructor: [ng.router_deprecated.Router, function(router) {
      }]
    });

})(window.app || (window.app = {}));
